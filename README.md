# Send Logs

Send ADIF formatted logs to LoTW and eQSL

##Config

Edit send_logs and replace/fill in the following lines

```#LoTW Station Location
TQSL_SL="" //Enter your Station Location from the TrustedQSL app.

#eQSL Login Info
USER="" //Enter your eQSL username
PSWD="" //Enter your eQSL password

#Location of ADIF formatted log files
LOG_FILES=( ~/.local/share/WSJT-X/wsjtx_log.adi ~/.local/share/JS8Call/js8call_log.adi ) //Location on your system of the ADIF log files.  Default listed are the JS8Call and WSJT-X locations.  Place a space between files
```

##Use (Manual Run)

`$ ./send_logs`

or copy to system search path

```
$ sudo cp send_logs /usr/local/bin/
$ send_logs
```

##Use (Cron Job)

(Optional) Copy the script to /usr/local/bin/

`$ sudo cp send_logs /usr/local/bin/send_logs`

Open your users crontab:

`$ crontab -e`

Add the following line to then end of this file:
`0 0 * * * /usr/local/bin/send_logs > /dev/null`

This will run the send_logs script every night at midnight.  Adjust as needed

